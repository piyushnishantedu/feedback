package com.cust.feedback.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cust.feedback.fragment.QuestionFragment;
import com.cust.feedback.listener.AnswerOptionListener;
import com.cust.feedback.listener.PageButtonClickListener;
import com.cust.feedback.modal.QuestionAnswer;

import java.util.List;

/**
 * Created by piyushnishant on 17/12/16.
 */

public class QuestionPageAdapter extends FragmentStatePagerAdapter {
    //static final int NUM_ITEMS = 10;
    private FragmentManager fragmentmanager;
    private List<QuestionAnswer> questionanswerlist;
    private AnswerOptionListener itemlistener;
    private PageButtonClickListener pageButtonclickLitener;

    public PageButtonClickListener getPageButtonclickLitener() {
        return pageButtonclickLitener;
    }

    public void setPageButtonclickLitener(PageButtonClickListener pageButtonclickLitener) {
        this.pageButtonclickLitener = pageButtonclickLitener;
    }

    public void setFragmentmanager(FragmentManager fragmentmanager) {
        this.fragmentmanager = fragmentmanager;
    }

    public FragmentManager getFragmentmanager() {
        return fragmentmanager;
    }

    public List<QuestionAnswer> getQuestionanswerlist() {
        return questionanswerlist;
    }

    public void setQuestionanswerlist(List<QuestionAnswer> questionanswerlist) {
        this.questionanswerlist = questionanswerlist;
    }

    public AnswerOptionListener getItemlistener() {
        return itemlistener;
    }

    public void setItemlistener(AnswerOptionListener itemlistener) {
        this.itemlistener = itemlistener;
    }

    public QuestionPageAdapter(FragmentManager fm, List<QuestionAnswer> questionanswerlist){
        super(fm);
        this.fragmentmanager = fm;
        this.questionanswerlist = questionanswerlist;
    }

    @Override
    public int getCount() {

        return questionanswerlist.size();
    }

    @Override
    public Fragment getItem(int position) {
        final int pos = position;
        QuestionAnswer questionanswerObject = questionanswerlist.get(position);
        QuestionFragment questionfragmentObject = new QuestionFragment();
        questionfragmentObject.setQuestionanswerObject(questionanswerObject);
        questionfragmentObject.setCurrentpostion(position);
        questionfragmentObject.setLastitem(this.questionanswerlist.size()-1);
        questionfragmentObject.setClicklistener(new AnswerOptionListener() {
            @Override
            public void optionClickListener(int id) {
                if(itemlistener != null){
                    itemlistener.optionClickListenerWithPosition(id,pos);
                }
            }

            @Override
            public void optionClickListenerWithPosition(int id, int position) {
                //

            }

            @Override
            public void submitButtonIsClicked(boolean isClicked) {
                if (isClicked){
                    if(itemlistener != null){
                        itemlistener.submitButtonIsClicked(true);
                    }
                }
            }
        });
        questionfragmentObject.setPageButtonlistener(new PageButtonClickListener() {
            @Override
            public void pageButtonisClicked(boolean isNextpageClicked) {
                if (pageButtonclickLitener != null){
                    pageButtonclickLitener.pageButtonisClicked(isNextpageClicked);
                }
            }
        });

        return questionfragmentObject;
    }
}


