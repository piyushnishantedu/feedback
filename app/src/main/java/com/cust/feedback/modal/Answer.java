package com.cust.feedback.modal;

/**
 * Created by piyushnishant on 23/12/16.
 */

public class Answer {
    private String answerId;
    private String answertext;

    public Answer(String answerId, String answertext) {
        this.answerId = answerId;
        this.answertext = answertext;
    }

    public String getAnswerId() {
        return answerId;
    }

    public String getAnswertext() {
        return answertext;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public void setAnswertext(String answertext) {
        this.answertext = answertext;
    }
}
