package com.cust.feedback.modal;

import java.util.List;

/**
 * Created by piyushnishant on 23/12/16.
 */

public class QuestionAnswerSingleton {
    private List<QuestionAnswer> questionAnswerList;
    private static QuestionAnswerSingleton questionAnswerObject;
    private QuestionAnswerSingleton(){

    }
    public static QuestionAnswerSingleton sharedInstance(){
        if (questionAnswerObject == null){
            questionAnswerObject = new QuestionAnswerSingleton();
        }
        return questionAnswerObject;
    }

    public List<QuestionAnswer> getQuestionAnswerList() {
        return questionAnswerList;
    }

    public void setQuestionAnswerList(List<QuestionAnswer> questionAnswerList) {
        this.questionAnswerList = questionAnswerList;
    }
}
