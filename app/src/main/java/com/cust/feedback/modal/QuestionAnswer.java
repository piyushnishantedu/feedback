package com.cust.feedback.modal;

import java.util.List;

/**
 * Created by piyushnishant on 17/12/16.
 */

public class QuestionAnswer {
    private List<Answer> answerList;
    private String questionID;
    private String questiontext;
    //private List<QuestionAnswer> questionanswerList;
   // private static QuestionAnswer questionANswerObject;

    public QuestionAnswer(){

    }
   /* public static QuestionAnswer sharedInstance(){
        if(questionANswerObject == null){
            questionANswerObject = new QuestionAnswer();
        }
        return questionANswerObject;

    }*/


    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getQuestiontext() {
        return questiontext;
    }

    public void setQuestiontext(String questiontext) {
        this.questiontext = questiontext;
    }
}
