package com.cust.feedback;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.feedback.common.Appconstant;
import com.cust.feedback.common.Helper;
import com.cust.feedback.utility.ConnectivityReceiver;
import com.cust.feedback.utility.CustomPostRequest;
import com.cust.feedback.utility.WAppController;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

public class OTPActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private AppCompatButton otpSubmitButton;
    private AppCompatButton otpResendButton;
    private SharedPreferences mPrefs;
    private EditText inputotp;
   // private String otpvalue = "123";
    private AVLoadingIndicatorView lodingIndicator;
    private RelativeLayout indicatorLayout;
    private HashMap<String,String> detailhashMap;
    private ScrollView formscrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        mPrefs = WAppController.getPreferences(this);
        inputotp = (EditText) findViewById(R.id.input_otp);
        otpSubmitButton = (AppCompatButton) findViewById(R.id.otpmsubmit_button);
        otpResendButton = (AppCompatButton) findViewById(R.id.otp_resend_button);
        formscrollview = (ScrollView) findViewById(R.id.form_scrollview);
        indicatorLayout = (RelativeLayout) findViewById(R.id.indicator_layout_rl);
        lodingIndicator = (AVLoadingIndicatorView) findViewById(R.id.avl_loading_indicator);
        otpSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ValidadteOpt(inputotp.getText().toString());

            }
        });
        lodingIndicator.smoothToHide();
        indicatorLayout.setVisibility(View.GONE);
        otpResendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Code To resend Otp

                if (ConnectivityReceiver.isConnected()){
                    indicatorLayout.setVisibility(View.VISIBLE);
                    lodingIndicator.smoothToShow();
                    formscrollview.setVisibility(View.GONE);
                    sendOtepRequest();
                }
                else {
                    indicatorLayout.setVisibility(View.GONE);
                    lodingIndicator.smoothToHide();
                    formscrollview.setVisibility(View.VISIBLE);
                    Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
                }
            }
        });
        Intent intent = getIntent();
        detailhashMap = (HashMap<String, String>) intent.getSerializableExtra("question_answer_map");
    }

    @Override
    protected void onResume() {
        super.onResume();
        configureFont();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected){
            Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
        }
    }

    //Valid Otp Code
    private void ValidadteOpt(String otp){
        if (!otp.isEmpty() && otp.equals(detailhashMap.get(Appconstant.otp))){

            if (ConnectivityReceiver.isConnected()){
                indicatorLayout.setVisibility(View.VISIBLE);
                lodingIndicator.smoothToShow();
                formscrollview.setVisibility(View.GONE);
                sendQuestionAnserToServer(detailhashMap);
            }
            else{
                indicatorLayout.setVisibility(View.GONE);
                lodingIndicator.smoothToHide();
                Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
                formscrollview.setVisibility(View.VISIBLE);
            }

        }
        else{
            Helper.showToast(getBaseContext(),"Otp is not valid",Toast.LENGTH_SHORT);
            indicatorLayout.setVisibility(View.GONE);
            lodingIndicator.smoothToHide();
            formscrollview.setVisibility(View.VISIBLE);
        }



    }


    //Send Question AnserId to Server
    private void sendQuestionAnserToServer(HashMap<String,String> peopledetailMap){
        JSONObject outerJson = new JSONObject();

        try {
            JSONObject dataObject = new JSONObject();
            dataObject.put(Appconstant.user_id,mPrefs.getString(Appconstant.user_id,""));

            JSONObject peopleDetailObject = new JSONObject();
            peopleDetailObject.put(Appconstant.name,peopledetailMap.get(Appconstant.name));
            peopleDetailObject.put(Appconstant.email_key,peopledetailMap.get(Appconstant.email_key));
            peopleDetailObject.put(Appconstant.mobile,peopledetailMap.get(Appconstant.mobile));
            peopleDetailObject.put(Appconstant.shop_name,peopledetailMap.get(Appconstant.shop_name));
            peopleDetailObject.put(Appconstant.shop_location,peopledetailMap.get(Appconstant.shop_location));

            JSONObject options = new JSONObject();


            Iterator myVeryOwnIterator = peopledetailMap.keySet().iterator();
            while(myVeryOwnIterator.hasNext()) {
                String key=(String)myVeryOwnIterator.next();
                String value=(String)peopledetailMap.get(key);
                if (!key.equals(Appconstant.otp)&&!key.equals(Appconstant.shop_location)&&!key.equals(Appconstant.shop_name)&&!key.equals(Appconstant.name) && !key.equals(Appconstant.email_key) && !key.equals(Appconstant.mobile)){
                    options.put(key,value);
                }
            }

            dataObject.put(Appconstant.peopledetails,peopleDetailObject);
            dataObject.put(Appconstant.questionans,options);
            outerJson.put(Appconstant.api_action_key,Appconstant.submit);
            outerJson.put(Appconstant.api_key_key,mPrefs.getString(Appconstant.api_key_key,""));
            outerJson.put(Appconstant.auth_type_key,Appconstant.login_auth_type);
            outerJson.put(Appconstant.data_key,dataObject);

        }catch (JSONException e){
            e.printStackTrace();

        }
        final String ApiURL = Appconstant.API_BASE_URL+outerJson.toString();
        //Log.d("URL",ApiURL);

        //Log.d("URL",ApiURL);
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.GET,
                ApiURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d("Quesresponse : ",""+response);
                if (response != null) {
                    try {
                        String result = response.getString("status");
                        if(result.equals(Appconstant.STATUS_SUCCESS_VALUE) && response.getInt("status_code") == Appconstant.SUCCESS_STATUS_CODE){

                            indicatorLayout.setVisibility(View.GONE);
                            lodingIndicator.smoothToHide();
                            Helper.showToast(getBaseContext(),response.getString("msg"),Toast.LENGTH_LONG);
                            Intent thankintent = new Intent(OTPActivity.this,ThankyouActivity.class);
                            startActivity(thankintent);
                            finish();


                        }else{
                            Helper.showToast(getBaseContext(),"Technical Error in else", Toast.LENGTH_SHORT);
                            indicatorLayout.setVisibility(View.GONE);
                            lodingIndicator.smoothToHide();
                            formscrollview.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Helper.showToast(getBaseContext(),"Technical Error in catch",Toast.LENGTH_SHORT);
                        indicatorLayout.setVisibility(View.GONE);
                        lodingIndicator.smoothToHide();
                        formscrollview.setVisibility(View.VISIBLE);
                        e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("vollyerror",""+error);
                Helper.showToast(getBaseContext(),"Technical Error in volley error",Toast.LENGTH_SHORT);
                indicatorLayout.setVisibility(View.GONE);
                lodingIndicator.smoothToHide();
                formscrollview.setVisibility(View.VISIBLE);
            }
        });
        WAppController.getInstance().addToRequestQueue(jsonReq);

    }
    //Send server for Otp request
    private void sendOtepRequest(){
        JSONObject outerJson = new JSONObject();

        try {
            JSONObject data = new JSONObject();
            data.put(Appconstant.email_key,detailhashMap.get(Appconstant.email_key));
            data.put(Appconstant.name,detailhashMap.get(Appconstant.name));
            data.put(Appconstant.mobile,detailhashMap.get(Appconstant.mobile));

            outerJson.put(Appconstant.api_action_key,Appconstant.otpaction);
            outerJson.put(Appconstant.api_key_key,mPrefs.getString(Appconstant.api_key_key,""));
            outerJson.put(Appconstant.auth_type_key,Appconstant.login_auth_type);
            outerJson.put(Appconstant.data_key,data);

        }catch (JSONException e){
            e.printStackTrace();

        }
        try{
            final String ApiURL = Appconstant.API_BASE_URL+ URLEncoder.encode(outerJson.toString(),"utf-8");
            Log.d("URL",ApiURL);
            CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.GET,
                    ApiURL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                     Log.d("resend : ",""+response);
                    if (response != null) {
                        try {
                            String result = response.getString("status");
                            if(result.equals(Appconstant.STATUS_SUCCESS_VALUE) && response.getInt("status_code") == Appconstant.SUCCESS_STATUS_CODE){
                                parseJsondata(response.getJSONObject(Appconstant.data_key));
                                Helper.showToast(getBaseContext(),response.getString("msg"),Toast.LENGTH_SHORT);

                            }else{
                                Helper.showToast(getBaseContext(),response.getString("msg"), Toast.LENGTH_SHORT);
                                lodingIndicator.smoothToHide();
                                indicatorLayout.setVisibility(View.GONE);
                                formscrollview.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Helper.showToast(getBaseContext(),"Technical Error in catch",Toast.LENGTH_SHORT);
                            lodingIndicator.smoothToHide();
                            indicatorLayout.setVisibility(View.GONE);
                            formscrollview.setVisibility(View.VISIBLE);
                            e.printStackTrace();}
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Log.d("vollyerror",""+error);
                    Helper.showToast(getBaseContext(),"Technical Error in v error",Toast.LENGTH_SHORT);
                    lodingIndicator.smoothToHide();
                    indicatorLayout.setVisibility(View.GONE);
                    formscrollview.setVisibility(View.VISIBLE);
                }
            });
            jsonReq.setShouldCache(false);
            WAppController.getInstance().addToRequestQueue(jsonReq);
        }catch(UnsupportedEncodingException e){
            Log.d("Encoding exception",""+e.getMessage());
        }

        //final String ApiURL = Appconstant.API_BASE_URL+outerJson.toString();
       // Log.d("URL",ApiURL);

    }

    private void parseJsondata(JSONObject dataobject){
        try{
            detailhashMap.put(Appconstant.otp,dataobject.getString(Appconstant.otp));
            lodingIndicator.smoothToHide();
            indicatorLayout.setVisibility(View.GONE);
            formscrollview.setVisibility(View.VISIBLE);


        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    /**
     * Configure UI
     */
    private void configureFont(){
        Helper.setCustomFontOnEditText(inputotp,this);
        Helper.setCustomFontOnAppConpactButton(otpSubmitButton,this);
        Helper.setCustomFontOnAppConpactButton(otpResendButton,this);

    }

}
