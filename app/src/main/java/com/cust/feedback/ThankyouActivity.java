package com.cust.feedback;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import com.cust.feedback.common.Helper;
import com.cust.feedback.utility.WAppController;

import java.util.Timer;
import java.util.TimerTask;

public class ThankyouActivity extends AppCompatActivity {
    private AppCompatButton startAgainButton;
    private TextView thankyoutext;
    private SharedPreferences.Editor mPrefEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        mPrefEditor = WAppController.getPreferences(this).edit();
        startAgainButton = (AppCompatButton) findViewById(R.id.startagain_button);
        thankyoutext = (TextView) findViewById(R.id.thankyou_text);
        startAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startagain = new Intent(ThankyouActivity.this,QuestionFragmentStatePagerSupport.class);
                startActivity(startagain);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Helper.setCustomFontOnTextView(thankyoutext,this);
        mPrefEditor.clear();
        mPrefEditor.commit();
        startLoginActivity();
    }

    private void startLoginActivity(){
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                Intent intent = new Intent(ThankyouActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        };
        Timer t = new Timer();
        t.schedule(task, 5000);
    }
}
