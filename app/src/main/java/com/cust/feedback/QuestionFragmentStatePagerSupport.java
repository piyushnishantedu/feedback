package com.cust.feedback;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.feedback.adapter.QuestionPageAdapter;
import com.cust.feedback.common.Appconstant;
import com.cust.feedback.common.CustomViewPager;
import com.cust.feedback.common.Helper;
import com.cust.feedback.listener.AnswerOptionListener;
import com.cust.feedback.listener.PageButtonClickListener;
import com.cust.feedback.modal.Answer;
import com.cust.feedback.modal.QuestionAnswer;
import com.cust.feedback.modal.QuestionAnswerSingleton;
import com.cust.feedback.utility.ConnectivityReceiver;
import com.cust.feedback.utility.CustomPostRequest;
import com.cust.feedback.utility.WAppController;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuestionFragmentStatePagerSupport extends AppCompatActivity  implements ConnectivityReceiver.ConnectivityReceiverListener {
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;

    private QuestionPageAdapter questionPageAdapter;
    private CustomViewPager questionViewPager;
    private List<QuestionAnswer> questionanswerlist  = new ArrayList<>();
    private HashMap<String,String> questionAnswerMap = new HashMap<>();
    private AVLoadingIndicatorView lodingIndicator;
    private RelativeLayout indicatorLayout;
    private boolean isQuestioOptionClicked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_fragment_state_pager_support);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        questionPageAdapter = new QuestionPageAdapter(getSupportFragmentManager(),questionanswerlist);
        questionViewPager = (CustomViewPager) findViewById(R.id.question_pager);
        indicatorLayout = (RelativeLayout) findViewById(R.id.indicator_layout_rl);
        lodingIndicator = (AVLoadingIndicatorView) findViewById(R.id.avl_loading_indicator);
        questionViewPager.setPagingEnabled(false);
        questionViewPager.setAdapter(questionPageAdapter);

        //questionViewPager.;
        questionPageAdapter.setItemlistener(new AnswerOptionListener() {
            @Override
            public void optionClickListener(int id) {

            }

            @Override
            public void optionClickListenerWithPosition(int id, int position) {
                //Log.d("id",""+id);
                //Log.d("position",""+position);
                isQuestioOptionClicked = true;
                storeQuestionAnswerMap(questionanswerlist.get(position),id);
                //Log.d("SIZe",""+questionAnswerMap.size());




            }

            @Override
            public void submitButtonIsClicked(boolean isClicked) {
                if (isClicked){
                    //Log.d("Submit",""+isClicked);
                }
            }
        });

        questionPageAdapter.setPageButtonclickLitener(new PageButtonClickListener() {
            @Override
            public void pageButtonisClicked(boolean isNextpageClicked) {
                if (isNextpageClicked && isQuestioOptionClicked){
                    isQuestioOptionClicked = false;
                    questionViewPager.setCurrentItem(questionViewPager.getCurrentItem()+1,false);
                    if (questionAnswerMap.size() == questionanswerlist.size()){
                        //Log.d("Question list",""+questionAnswerMap);
                        Intent formintent = new Intent(QuestionFragmentStatePagerSupport.this,FormSubmitActivity.class);
                        formintent.putExtra("question_answer_map", questionAnswerMap);
                        startActivity(formintent);
                        finish();
                        //count = 0;

                    }

                }
                else {
                    //questionViewPager.setCurrentItem(questionViewPager.getCurrentItem()-1,false);
                    Helper.showToast(getBaseContext(),"Please Select an option",Toast.LENGTH_SHORT);
                }
            }
        });
        lodingIndicator.smoothToShow();


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (questionanswerlist.size()>0){
            questionanswerlist.clear();
        }
        if (QuestionAnswerSingleton.sharedInstance().getQuestionAnswerList()!= null){
            for (int i = 0;i<QuestionAnswerSingleton.sharedInstance().getQuestionAnswerList().size();i++){
                this.questionanswerlist.add(QuestionAnswerSingleton.sharedInstance().getQuestionAnswerList().get(i));
            }
            questionPageAdapter.notifyDataSetChanged();
            lodingIndicator.smoothToHide();
            indicatorLayout.setVisibility(View.GONE);
        }
        else{
            if (ConnectivityReceiver.isConnected()) {
                getQuestionAnswerListFromServer();
            }
            else {
                Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
            }
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected){
            Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
        }
    }

    /**
     * Get question answer list
     */
    private void getQuestionAnswerListFromServer(){
        JSONObject outerJson = new JSONObject();

        try {
            outerJson.put(Appconstant.api_action_key,Appconstant.questionanswerlist_api_action);
            outerJson.put(Appconstant.api_key_key,Appconstant.default_api_key);
            outerJson.put(Appconstant.auth_type_key,Appconstant.default_auth_type);


        }catch (JSONException e){
            e.printStackTrace();

        }
        final String ApiURL = Appconstant.API_BASE_URL+outerJson.toString();
       // Log.d("URL",ApiURL);
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.GET,
                ApiURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d("Quesresponse : ",""+response);
                if (response != null) {
                    try {
                        String result = response.getString("status");
                        if(result.equals(Appconstant.STATUS_SUCCESS_VALUE) && response.getInt("status_code") == Appconstant.SUCCESS_STATUS_CODE){
                            parseQuestionAnswerJsonData(response.getJSONObject(Appconstant.data_key));

                        }else{
                            Helper.showToast(getBaseContext(),"Technical Error in else", Toast.LENGTH_SHORT);
                        }
                    } catch (JSONException e) {
                        Helper.showToast(getBaseContext(),"Technical Error in catch",Toast.LENGTH_SHORT);
                        e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Log.d("vollyerror",""+error);
                Helper.showToast(getBaseContext(),"Technical Error in volley error",Toast.LENGTH_SHORT);
            }
        });
        WAppController.getInstance().addToRequestQueue(jsonReq);

    }
    //Parse Question answer list
    private void parseQuestionAnswerJsonData(JSONObject datajsonObject){
        try {
            JSONArray questionListArray = datajsonObject.getJSONArray(Appconstant.questionlist);
            JSONArray anserListArray = datajsonObject.getJSONArray(Appconstant.answerlist);
            List<QuestionAnswer> questionanswerlist = new ArrayList<>();
            List<Answer> anwerlist = new ArrayList<>();
            for (int j = 0;j<anserListArray.length();j++){
                JSONObject jsonanswerObject = anserListArray.getJSONObject(j);
                Answer ansObj = new Answer(jsonanswerObject.getString(Appconstant.answer_id),jsonanswerObject.getString(Appconstant.answer_text));
                anwerlist.add(ansObj);
            }

            for (int i = 0;i<questionListArray.length();i++){
                JSONObject questionObject = questionListArray.getJSONObject(i);
                QuestionAnswer obj = new QuestionAnswer();
                obj.setAnswerList(anwerlist);
                obj.setQuestionID(questionObject.getString(Appconstant.question_id));
                obj.setQuestiontext(questionObject.getString(Appconstant.question_text));
                questionanswerlist.add(obj);
            }

            QuestionAnswerSingleton questionAnswerSingletonObject = QuestionAnswerSingleton.sharedInstance();
            questionAnswerSingletonObject.setQuestionAnswerList(questionanswerlist);
            for (int i = 0;i<questionAnswerSingletonObject.getQuestionAnswerList().size();i++){
                this.questionanswerlist.add(questionAnswerSingletonObject.getQuestionAnswerList().get(i));
            }
            questionPageAdapter.notifyDataSetChanged();
            //lodingIndicator.setVisibility(View.GONE);
            lodingIndicator.smoothToHide();
            indicatorLayout.setVisibility(View.GONE);


        }catch (JSONException e){
            e.printStackTrace();
        }

    }
    //Store Question ANswer Data
    private void storeQuestionAnswerMap(QuestionAnswer questionObj,int optionId){
        questionAnswerMap.put(questionObj.getQuestionID(),String.valueOf(optionId));
    }



}
