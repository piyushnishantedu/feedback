package com.cust.feedback;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;

import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.feedback.common.Appconstant;
import com.cust.feedback.common.Helper;
import com.cust.feedback.modal.Answer;
import com.cust.feedback.modal.QuestionAnswer;
import com.cust.feedback.modal.QuestionAnswerSingleton;
import com.cust.feedback.utility.ConnectivityReceiver;
import com.cust.feedback.utility.CustomPostRequest;
import com.cust.feedback.utility.WAppController;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private AppCompatButton loginButton;
    private EditText inputemail;
    private EditText inputpassword;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private AVLoadingIndicatorView lodingIndicator;
    private RelativeLayout indicatorLayout;
    private ScrollView loginformscrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        loginButton = (AppCompatButton) findViewById(R.id.btn_login);
        inputemail = (EditText) findViewById(R.id.input_email);
        inputpassword = (EditText) findViewById(R.id.input_password);
        indicatorLayout = (RelativeLayout) findViewById(R.id.indicator_layout_rl);
        lodingIndicator = (AVLoadingIndicatorView) findViewById(R.id.avl_loading_indicator);
        loginformscrollview = (ScrollView) findViewById(R.id.login_form_scrollview);
        loginformscrollview.setVisibility(View.GONE);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginuser();
            }
        });
        //getQuestionAnswerListFromServer();
        mPrefEditor.clear();
        mPrefEditor.commit();

    }
    @Override
    protected void onResume() {
        super.onResume();
        String userid = mPrefs.getString(Appconstant.user_id,"");
        String apikey = mPrefs.getString(Appconstant.api_key_key,"");
        if (!userid.isEmpty() && !apikey.isEmpty()){
            gotoQuestionActivity();
        }
        else{
            indicatorLayout.setVisibility(View.GONE);
            lodingIndicator.smoothToHide();
            loginformscrollview.setVisibility(View.VISIBLE);
        }
        Helper.setCustomFontOnAppConpactButton(loginButton,this);
        Helper.setCustomFontOnEditText(inputemail,this);
        Helper.setCustomFontOnEditText(inputpassword,this);

    }
//Check Network Connection

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected){
            Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
        }

    }

    private void gotoQuestionActivity(){
        lodingIndicator.smoothToHide();
        indicatorLayout.setVisibility(View.GONE);
        Intent questionAcivity = new Intent(LoginActivity.this,QuestionFragmentStatePagerSupport.class);
        startActivity(questionAcivity);
        finish();

    }


    //Validation
    private void loginuser(){
        //boolean isvalid = false;
        if (Helper.isValidEmail(inputemail.getText().toString())){
            if (!inputpassword.getText().toString().isEmpty()){
                if (ConnectivityReceiver.isConnected()){
                    loginformscrollview.setVisibility(View.GONE);
                    indicatorLayout.setVisibility(View.VISIBLE);
                    lodingIndicator.smoothToShow();
                    aunthanticateUser();
                }
                else{
                    Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
                }

            }
            else {
                Helper.showToast(getBaseContext(),"Password is Empity",Toast.LENGTH_SHORT);
            }
        }
        else {
            Helper.showToast(getBaseContext(),"Email is Not Valid",Toast.LENGTH_SHORT);
        }
        //return isvalid;
    }

    //Aunthicate Login Credential
    private void aunthanticateUser(){
        JSONObject outerJson = new JSONObject();

        try {
            JSONObject data = new JSONObject();
            data.put(Appconstant.email_key,inputemail.getText().toString());
            data.put(Appconstant.password_key,inputpassword.getText().toString());


            outerJson.put(Appconstant.api_action_key,Appconstant.api_action_login);
            outerJson.put(Appconstant.api_key_key,Appconstant.default_api_key);
            outerJson.put(Appconstant.auth_type_key,Appconstant.default_auth_type);
            outerJson.put(Appconstant.data_key,data);

        }catch (JSONException e){
            e.printStackTrace();

        }
        final String ApiURL = Appconstant.API_BASE_URL+outerJson.toString();
        //Log.d("URL",ApiURL);
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.GET,
                ApiURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d("Login : ",""+response);
                if (response != null) {
                    try {
                        String result = response.getString("status");
                        if(result.equals(Appconstant.STATUS_SUCCESS_VALUE) && response.getInt("status_code") == Appconstant.SUCCESS_STATUS_CODE){
                            //parseQuestionAnswerJsonData(response.getJSONObject(Appconstant.data_key));
                            JSONObject dataObject = response.getJSONObject(Appconstant.data_key);
                            mPrefEditor.putString(Appconstant.api_key_key,dataObject.getString(Appconstant.api_key_key));
                            mPrefEditor.putString(Appconstant.user_id,dataObject.getString(Appconstant.user_id));
                            mPrefEditor.commit();
                            gotoQuestionActivity();

                        }else{
                            Helper.showToast(getBaseContext(),response.getString("msg"), Toast.LENGTH_SHORT);
                            lodingIndicator.smoothToHide();
                            indicatorLayout.setVisibility(View.GONE);
                            loginformscrollview.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Helper.showToast(getBaseContext(),"Technical Error in catch",Toast.LENGTH_SHORT);
                        lodingIndicator.smoothToHide();
                        indicatorLayout.setVisibility(View.GONE);
                        loginformscrollview.setVisibility(View.VISIBLE);
                        e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("vollyerror",""+error);
                Helper.showToast(getBaseContext(),"Technical Error in v error",Toast.LENGTH_SHORT);
                lodingIndicator.smoothToHide();
                indicatorLayout.setVisibility(View.GONE);
                loginformscrollview.setVisibility(View.VISIBLE);
            }
        });
        WAppController.getInstance().addToRequestQueue(jsonReq);

    }

}

