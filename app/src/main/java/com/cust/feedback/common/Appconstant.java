package com.cust.feedback.common;

/**
 * Created by piyushnishant on 23/12/16.
 */

public final class Appconstant {
    public static final String API_BASE_URL = "http://rohit.welcomeinweb.com/api?req=";
    public final static String STATUS_SUCCESS_VALUE = "success";
    public final static int SUCCESS_STATUS_CODE = 200;
    public static final String api_action_key = "api_action";
    public static final String api_action_login = "login";
    public static final String api_key_key = "api_key";
    public static final String default_api_key = "ZmVlZGJhY2tfMTIzNDU2";
    public static final String auth_type_key = "auth_type";
    public static final String default_auth_type = "1";
    public static final String login_auth_type = "2";
    public static final String data_key = "data";
    //LOGIN KEY
    public static final String email_key = "email";
    public static final String password_key = "password";
    //Question Answer list key
    public static final String questionanswerlist_api_action = "questionanswerlist";
    public static final String questionlist = "questionlist";
    public static final String question_id = "question_id";
    public static final String question_text = "question_text";
    public static final String answerlist = "answerlist";
    public static final String answer_id = "answer_id";
    public static final String answer_text = "answer_text";
    //USER Detail
    public static final String user_id = "user_id";
    //Form Submit
    public static final String submit = "submit";
    public static final String peopledetails = "peopledetails";
    public static final String name = "name";
    public static final String mobile = "mobile";
    public static final String questionans = "questionans";
    public static final String shop_name = "shop_name";
    public static final String shop_location = "location";
    //OTP Request
    public static final String otpaction = "peopledetail";
    public static final String otp = "otp";


}
