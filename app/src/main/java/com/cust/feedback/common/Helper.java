package com.cust.feedback.common;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by piyushnishant on 23/12/16.
 */

public class Helper {
    public static void showToast(Context context, String message, int duration){
        Toast messageToast = Toast.makeText(context,message,duration);
        messageToast.show();
    }
    //Email Validation
    public  static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    //Mobile Number
    public static boolean isValidMobile(CharSequence target){
        if (target == null){
            return false;
        }
        else if(target.length() == 10){
            return true;
        }
        else {
            return false;
        }

    }

    /**
     * Get Custom Font
     * @param context
     * @return
     */
    private static Typeface getCustomFont(Context context){
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLight.ttf");
        return custom_font;
    }
    public static void setCustomFontOnTextView(TextView textview,Context context){
        textview.setTypeface(getCustomFont(context));
    }
    public static void setCustomFontOnRadioButton(RadioButton radiobutton,Context context){
        radiobutton.setTypeface(getCustomFont(context));
    }
    public static void setCustomFontOnButton(Button button,Context context){
        button.setTypeface(getCustomFont(context));
    }
    public static void setCustomFontOnAppConpactButton(AppCompatButton appcompactbutton,Context context){
        appcompactbutton.setTypeface(getCustomFont(context));
    }
    public static void setCustomFontOnEditText(EditText edittext,Context context){
        edittext.setTypeface(getCustomFont(context));
    }
}
