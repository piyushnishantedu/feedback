package com.cust.feedback;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.feedback.common.Appconstant;
import com.cust.feedback.common.Helper;
import com.cust.feedback.utility.ConnectivityReceiver;
import com.cust.feedback.utility.CustomPostRequest;
import com.cust.feedback.utility.WAppController;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FormSubmitActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
   // private EditText commentText;
    private EditText nameText;
    private EditText mobilenumber;
    private EditText emailid;
    private AppCompatButton submitButton;
    private EditText shopName;
    private EditText shopLocation;
    private AVLoadingIndicatorView lodingIndicator;
    private RelativeLayout indicatorLayout;
    private SharedPreferences mPrefs;
    private HashMap<String,String> detailhashMap;
    private ScrollView formscrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_submit);
        mPrefs = WAppController.getPreferences(this);
        //commentText = (EditText) findViewById(R.id.input_comment);
        nameText = (EditText) findViewById(R.id.input_name);
        mobilenumber = (EditText) findViewById(R.id.input_mobile);
        emailid = (EditText) findViewById(R.id.input_email);
        shopName = (EditText) findViewById(R.id.input_shopName);
        shopLocation = (EditText) findViewById(R.id.input_location);
        submitButton = (AppCompatButton) findViewById(R.id.formsubmit_button);
        formscrollview = (ScrollView) findViewById(R.id.form_scrollview);
        indicatorLayout = (RelativeLayout) findViewById(R.id.indicator_layout_rl);
        lodingIndicator = (AVLoadingIndicatorView) findViewById(R.id.avl_loading_indicator);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Code to submit form
                storeCustomerInformation();

            }
        });

        indicatorLayout.setVisibility(View.GONE);
        lodingIndicator.smoothToHide();

    }

    @Override
    protected void onResume() {
        super.onResume();
        configureFont();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected){
            Helper.showToast(getBaseContext(),"Internet is Not Available",Toast.LENGTH_SHORT);
        }

    }

    //Store Customer Information
    private void storeCustomerInformation(){

        if (nameText.getText().toString().isEmpty()){
            Helper.showToast(getBaseContext(),"Please fill the Name", Toast.LENGTH_SHORT);
            return;
        }
        if (!Helper.isValidMobile(mobilenumber.getText().toString())){
            Helper.showToast(getBaseContext(),"Mobile Number is Not valid", Toast.LENGTH_SHORT);
            return;
        }
        if (!Helper.isValidEmail(emailid.getText().toString())){
            Helper.showToast(getBaseContext(),"Email  is Not valid", Toast.LENGTH_SHORT);
            return;

        }
        if (shopName.getText().toString().isEmpty()){
            Helper.showToast(getBaseContext(),"Please fill the Shop Name", Toast.LENGTH_SHORT);
            return;
        }
        if (shopLocation.getText().toString().isEmpty()){
            Helper.showToast(getBaseContext(),"Please fill the Shop Location", Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = getIntent();
        detailhashMap = (HashMap<String, String>) intent.getSerializableExtra("question_answer_map");
        detailhashMap.put(Appconstant.name,nameText.getText().toString());
        detailhashMap.put(Appconstant.mobile,mobilenumber.getText().toString());
        detailhashMap.put(Appconstant.email_key,emailid.getText().toString());
        detailhashMap.put(Appconstant.shop_name,shopName.getText().toString());
        detailhashMap.put(Appconstant.shop_location,shopLocation.getText().toString());
        if (ConnectivityReceiver.isConnected()){
            indicatorLayout.setVisibility(View.VISIBLE);
            lodingIndicator.smoothToShow();
            formscrollview.setVisibility(View.GONE);
            sendOtepRequest();
        }


    }
//Send server for Otp request
    private void sendOtepRequest(){
        JSONObject outerJson = new JSONObject();

        try {
            JSONObject data = new JSONObject();
            data.put(Appconstant.email_key,detailhashMap.get(Appconstant.email_key));
            data.put(Appconstant.name,detailhashMap.get(Appconstant.name));
            data.put(Appconstant.mobile,detailhashMap.get(Appconstant.mobile));

            outerJson.put(Appconstant.api_action_key,Appconstant.otpaction);
            outerJson.put(Appconstant.api_key_key,mPrefs.getString(Appconstant.api_key_key,""));
            outerJson.put(Appconstant.auth_type_key,Appconstant.login_auth_type);
            outerJson.put(Appconstant.data_key,data);

        }catch (JSONException e){
            e.printStackTrace();

        }
        try{
            final String ApiURL = Appconstant.API_BASE_URL+ URLEncoder.encode(outerJson.toString(),"utf-8");
            Log.d("URL",ApiURL);
            CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.GET,
                    ApiURL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Login : ",""+response);
                    if (response != null) {
                        try {
                            String result = response.getString("status");
                            if(result.equals(Appconstant.STATUS_SUCCESS_VALUE) && response.getInt("status_code") == Appconstant.SUCCESS_STATUS_CODE){
                                parseJsondata(response.getJSONObject(Appconstant.data_key));
                                Helper.showToast(getBaseContext(),response.getString("msg"),Toast.LENGTH_LONG);

                            }else{
                                Helper.showToast(getBaseContext(),response.getString("msg"), Toast.LENGTH_SHORT);
                                lodingIndicator.smoothToHide();
                                indicatorLayout.setVisibility(View.GONE);
                                formscrollview.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Helper.showToast(getBaseContext(),"Technical Error in catch",Toast.LENGTH_SHORT);
                            lodingIndicator.smoothToHide();
                            indicatorLayout.setVisibility(View.GONE);
                            formscrollview.setVisibility(View.VISIBLE);
                            e.printStackTrace();}
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // Log.d("vollyerror",""+error);
                    Helper.showToast(getBaseContext(),"Technical Error in v error",Toast.LENGTH_SHORT);
                    lodingIndicator.smoothToHide();
                    indicatorLayout.setVisibility(View.GONE);
                    formscrollview.setVisibility(View.VISIBLE);
                }
            });
            WAppController.getInstance().addToRequestQueue(jsonReq);
        }catch (UnsupportedEncodingException e){
            Log.d("Encoding exception",""+e.getMessage());
        }




    }

    private void parseJsondata(JSONObject dataobject){
        try{
            detailhashMap.put(Appconstant.otp,dataobject.getString(Appconstant.otp));
            lodingIndicator.smoothToHide();
            indicatorLayout.setVisibility(View.GONE);
            //Log.d("questionanswerMap",""+questionanswerMap);
            Intent questionIntent = new Intent(FormSubmitActivity.this,OTPActivity.class);
            questionIntent.putExtra("question_answer_map", detailhashMap);
            startActivity(questionIntent);
            finish();

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Configure UI
     */
    private void configureFont(){
        Helper.setCustomFontOnEditText(nameText,this);
        Helper.setCustomFontOnEditText(mobilenumber,this);
        Helper.setCustomFontOnEditText(emailid,this);
        Helper.setCustomFontOnEditText(shopName,this);
        Helper.setCustomFontOnAppConpactButton(submitButton,this);
        Helper.setCustomFontOnEditText(shopLocation,this);

    }

}
