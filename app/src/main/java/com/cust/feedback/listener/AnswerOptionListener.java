package com.cust.feedback.listener;

/**
 * Created by piyushnishant on 17/12/16.
 */

public interface AnswerOptionListener {
    void optionClickListener(int id);
    void optionClickListenerWithPosition(int id,int position);
    void submitButtonIsClicked(boolean isClicked);
}
