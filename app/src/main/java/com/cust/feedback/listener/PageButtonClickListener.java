package com.cust.feedback.listener;

/**
 * Created by piyushnishant on 23/12/16.
 */

public interface PageButtonClickListener {
    void pageButtonisClicked(boolean isNextpageClicked);
}
