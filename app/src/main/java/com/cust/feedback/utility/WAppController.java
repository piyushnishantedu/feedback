package com.cust.feedback.utility;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import org.acra.ACRA;

/**
 * Created by piyushnishant on 19/12/16.
 */

public class WAppController extends Application {
    public static final String TAG = WAppController.class.getSimpleName();
    public static volatile Context applicationContext;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private LruBitmapCache mLruBitmapCache;
    private static SharedPreferences mPrefs;
    private static SharedPreferences.Editor mPrefsEditor;
    private static WAppController mInstance;
    public static ContentResolver cr;
    //private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        ACRA.init(this);
        mInstance = this;
        cr = this.getContentResolver();
    }

    public static synchronized WAppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            //mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new OkHttpStack());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }
        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static SharedPreferences getPreferences(Context context){
        if (mPrefs == null || mPrefsEditor == null){
            mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            mPrefsEditor = mPrefs.edit();
        }
        return mPrefs;
    }
    //It never get called so need to cancel requests in activity
	/*@Override
	public void onTerminate() {
		super.onTerminate();
		mRequestQueue.cancelAll(TAG);
	}*/
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
