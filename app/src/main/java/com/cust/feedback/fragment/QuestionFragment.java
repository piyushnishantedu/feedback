package com.cust.feedback.fragment;

import android.media.Image;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cust.feedback.R;
import com.cust.feedback.common.Helper;
import com.cust.feedback.listener.AnswerOptionListener;
import com.cust.feedback.listener.PageButtonClickListener;
import com.cust.feedback.modal.QuestionAnswer;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by piyushnishant on 17/12/16.
 */

public class QuestionFragment extends Fragment {

    private QuestionAnswer questionanswerObject;
    private AnswerOptionListener clicklistener;
    private PageButtonClickListener pageButtonlistener;
    private int lastposition;
    private int currentpostion;
    //private int position;


    public void setLastitem(int lastitem) {
        this.lastposition = lastitem;
    }

    public void setCurrentpostion(int currentpostion) {
        this.currentpostion = currentpostion;
    }

    public AnswerOptionListener getClicklistener() {
        return clicklistener;
    }

    public PageButtonClickListener getPageButtonlistener() {
        return pageButtonlistener;
    }

    public void setPageButtonlistener(PageButtonClickListener pageButtonlistener) {
        this.pageButtonlistener = pageButtonlistener;
    }

    public void setClicklistener(AnswerOptionListener clicklistener) {
        this.clicklistener = clicklistener;
    }

    public QuestionAnswer getQuestionanswerObject() {
        return questionanswerObject;
    }

    public void setQuestionanswerObject(QuestionAnswer questionanswerObject) {
        this.questionanswerObject = questionanswerObject;
    }
/* static QuestionFragment newInstance(QuestionAnswer questionanswerObject){

        QuestionFragment obj = new QuestionFragment();

        return obj;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

         TextView questiontextView;
         RadioGroup questionOptionGroup;
         RadioButton option1RadioButton;
         RadioButton option2RadioButton;
         RadioButton option3RadioButton;
         RadioButton option4RadioButton;
        Button nextPageButton;
       // ImageButton previousPageButton;
       // Button submitButton;
        View view = inflater.inflate(R.layout.question_page_list,container,false);
        questiontextView = (TextView) view.findViewById(R.id.question_textview);
        questionOptionGroup = (RadioGroup) view.findViewById(R.id.radiobutton_group);
        option1RadioButton = (RadioButton) view.findViewById(R.id.option1);
        option2RadioButton = (RadioButton) view.findViewById(R.id.option2);
        option3RadioButton = (RadioButton) view.findViewById(R.id.option3);
        option4RadioButton = (RadioButton) view.findViewById(R.id.option4);
        nextPageButton = (Button) view.findViewById(R.id.page_next_image_button);
       // previousPageButton = (ImageButton) view.findViewById(R.id.page_previous_image_button);

        //Set Custom font
        Helper.setCustomFontOnTextView(questiontextView,getContext());
        Helper.setCustomFontOnRadioButton(option1RadioButton,getContext());
        Helper.setCustomFontOnRadioButton(option2RadioButton,getContext());
        Helper.setCustomFontOnRadioButton(option3RadioButton,getContext());
        Helper.setCustomFontOnRadioButton(option4RadioButton,getContext());
        Helper.setCustomFontOnButton(nextPageButton,getContext());

        //END



        //submitButton = (Button) view.findViewById(R.id.submit_button);

        option1RadioButton.setId(Integer.parseInt(questionanswerObject.getAnswerList().get(0).getAnswerId()));
        option2RadioButton.setId(Integer.parseInt(questionanswerObject.getAnswerList().get(1).getAnswerId()));
        option3RadioButton.setId(Integer.parseInt(questionanswerObject.getAnswerList().get(2).getAnswerId()));
        option4RadioButton.setId(Integer.parseInt(questionanswerObject.getAnswerList().get(3).getAnswerId()));

        if (questionanswerObject != null){
            questiontextView.setText("Q "+(currentpostion+1)+".  "+questionanswerObject.getQuestiontext());
            option1RadioButton.setText(questionanswerObject.getAnswerList().get(0).getAnswertext());
            option2RadioButton.setText(questionanswerObject.getAnswerList().get(1).getAnswertext());
            option3RadioButton.setText(questionanswerObject.getAnswerList().get(2).getAnswertext());
            option4RadioButton.setText(questionanswerObject.getAnswerList().get(3).getAnswertext());


        }
        questionOptionGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked){
                    if (clicklistener != null){
                        clicklistener.optionClickListener(checkedId);
                    }
                }
            }
        });

        nextPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageButtonlistener != null){
                    pageButtonlistener.pageButtonisClicked(true);
                }
            }
        });



        return view;
    }
}
